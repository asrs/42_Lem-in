/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_algo.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 19:16:09 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/24 15:57:27 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static bool			change_graph(int act, int i, int sw, t_graph *graph)
{
	int				j;

	j = 0;
	if (sw == 1)
	{
		while (j < (int)(graph->len - 1))
		{
			graph->matr[j][i] = 2;
			j++;
		}
	}
	graph->path[graph->pind] = act;
	graph->pind++;
	return (true);
}

static bool			check_end(int i, int pind, t_graph *graph)
{
	if (graph->matr[i][graph->len - 1] == 1)
	{
		graph->path[pind + 1] = (int)graph->len - 1;
		return (true);
	}
	return (false);
}

static bool			check_room(int act, t_graph *graph)
{
	int				i;
	int				n;

	i = (graph->len > 1) ? (int)graph->len - 1 : 1;
	n = (graph->pind < 1) ? 1 : graph->pind;
	if (check_end(act, graph->pind, graph) == true)
		return (change_graph(act, i, 0, graph));
	while (i > 0)
	{
		if (graph->matr[act][i] == 1)
			break ;
		i--;
	}
	if (i != 0)
	{
		change_graph(act, i, 1, graph);
		return (check_room(i, graph));
	}
	if (i <= 0 && act <= 0)
		return (false);
	graph->pind = (n > 0) ? n - 1 : 0;
	return (check_room(n - 1, graph));
}

bool				m_algo(t_graph *graph)
{
	if (check_room(0, graph))
		return (true);
	return (false);
}
