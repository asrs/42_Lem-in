/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_process.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/20 19:26:33 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/22 16:26:12 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void			get_id(t_data *data, t_graph *graph)
{
	t_room			*tmp;
	size_t			i;

	i = 0;
	tmp = data->rooms;
	while (i < graph->len)
	{
		graph->id[i] = ft_strdup(tmp->name);
		tmp = tmp->next;
		i++;
	}
}

static void			get_matr(t_data *data, t_graph *graph)
{
	int				i;
	int				j;
	t_pipe			*tmp;

	i = 0;
	j = 0;
	tmp = data->pipes;
	while (tmp->next)
	{
		i = ft_seek_id(tmp->in, graph);
		j = ft_seek_id(tmp->out, graph);
		graph->matr[i][j] = 1;
		graph->matr[j][i] = 1;
		tmp = tmp->next;
	}
	i = ft_seek_id(tmp->in, graph);
	j = ft_seek_id(tmp->out, graph);
	graph->matr[i][j] = 1;
	graph->matr[j][i] = 1;
}

static inline void	check_loop(t_graph *graph)
{
	size_t			i;

	i = 0;
	while (i < graph->len)
	{
		if (graph->matr[i][0] == 1)
			graph->matr[i][0] = 0;
		if (graph->matr[i][i] == 1)
			graph->matr[i][i] = 0;
		i++;
	}
}

static void			create_ant(t_data *data, t_graph *graph)
{
	size_t			i;

	i = 0;
	while (i < data->nb_ant)
	{
		graph->ants[i].pos = 0;
		graph->ants[i].id = ft_strjoin_free(graph->ants[i].id, \
				ft_itoa((int)i + 1), 3);
		i++;
	}
}

void				m_process(t_data *data, t_graph *graph)
{
	get_id(data, graph);
	get_matr(data, graph);
	check_loop(graph);
	create_ant(data, graph);
	if (data->nb_room > 10000)
		ft_message(2, data, graph);
}
