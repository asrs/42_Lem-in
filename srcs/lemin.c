/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 14:59:08 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/24 15:56:42 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static inline bool		init_data(t_data *data)
{
	if (!(data->readed = ft_strnew(1)))
		return (false);
	data->nb_ant = 0;
	data->nb_room = 0;
	data->nb_link = 0;
	data->next_name = 0;
	data->next_type = NEUTRE;
	data->rooms = NULL;
	data->pipes = NULL;
	data->get_ant = false;
	data->get_start = false;
	data->get_end = false;
	return (true);
}

static inline bool		init_graph(t_data *data, t_graph *graph)
{
	size_t				i;

	i = 0;
	graph->len = data->nb_room;
	graph->pind = 0;
	graph->nb_ant = data->nb_ant;
	if (!(graph->matr = (int **)malloc(sizeof(int *) * graph->len)))
		return (false);
	if (!(graph->ants = (t_ant *)malloc(sizeof(t_ant) * data->nb_ant)))
		return (false);
	graph->id = ft_str_tabmake(graph->len);
	graph->path = ft_int_tabmake(graph->len);
	while (i < graph->len)
	{
		graph->matr[i] = ft_int_tabmake(graph->len);
		i++;
	}
	i = 0;
	while (i < data->nb_ant)
	{
		graph->ants[i].id = ft_strnew(1);
		graph->ants[i].id[0] = 'L';
		i++;
	}
	return (true);
}

int						main(void)
{
	t_data		data;
	t_graph		graph;

	if (!init_data(&data))
		ft_message(2, &data, &graph);
	m_parse(&data);
	if (!init_graph(&data, &graph))
		ft_message(2, &data, &graph);
	m_process(&data, &graph);
	(m_algo(&graph) == true) ? m_print(&graph) : ft_putendl("NO PATH");
	ft_message(0, &data, &graph);
	return (EXIT_SUCCESS);
}
