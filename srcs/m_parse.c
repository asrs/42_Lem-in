/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_parse.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 14:59:17 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/21 16:01:19 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static bool			get_cmd(char *line, t_data *data)
{
	if (ft_strstr(line, "start") != NULL && data->get_start == false)
	{
		data->next_type = START;
		data->get_start = true;
		return (true);
	}
	else if (ft_strstr(line, "end") != NULL && data->get_end == false)
	{
		data->next_type = END;
		data->get_end = true;
		return (true);
	}
	return (false);
}

static bool			get_ant(t_data *data, char *line)
{
	int				rd;
	int				ret;
	long long		check;

	ret = 0;
	check = 0;
	if ((rd = get_next_line(0, &line)) < 1 || line == NULL || line[0] == '\0')
		ret = 1;
	(ret == 0) ? ft_stock(line, data) : 0;
	if (line && ft_is_str_n(line) == false)
		ret = 1;
	if (ret != 1)
		check = ft_atoll(line);
	if (ret != 1 && (check < -2146473648 || check > 2146473647 || check == 0))
		ret = 1;
	if (ret != 1)
	{
		data->get_ant = true;
		data->nb_ant = (unsigned int)check;
	}
	if (ret == 1 && line && line[0] == '#')
		ret = 0;
	(line) ? ft_strdel(&line) : 0;
	return ((ret == 0) ? true : false);
}

static bool			get_data(int *n, t_data *data, char *line)
{
	int				rd;
	int				ret;

	ret = 0;
	if ((rd = get_next_line(0, &line)) < 1 || !line || ft_strequ(line, "\0"))
		ret = 1;
	(ret == 0) ? ft_stock(line, data) : ft_strdel(&line);
	if (line && line[0] == '#' && line[1] == '#')
		get_cmd(line, data);
	else if (ret == 0 && line && p_room(line, data) && *n == 0)
		ret = (m_room(line, data) == true) ? 0 : 1;
	else if ((line && p_room(line, data) && *n == 1) || \
			(line && !p_room(line, data) && !p_pipe(line, data)))
		ret = 1;
	else if (line && p_pipe(line, data))
	{
		*n = 1;
		ret = (m_pipe(line, data) == true) ? 0 : 1;
	}
	if (ret == 1 && line != NULL && line[0] == '#' && line[1] != '#')
		ret = 0;
	(line) ? ft_strdel(&line) : 0;
	return ((ret == 0) ? true : false);
}

void				m_parse(t_data *data)
{
	int				i;
	int				n;

	i = 0;
	n = 0;
	while (data->get_ant == false)
	{
		if (!get_ant(data, NULL))
		{
			ft_message(2, data, NULL);
			return ;
		}
	}
	while (get_data(&n, data, NULL) == true)
		i++;
	if (m_valid(data))
		ft_putendl(data->readed);
	else
		ft_message(2, data, NULL);
}
