/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_room.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 12:03:11 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/23 17:03:43 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static bool			check_coord(char **tab, t_data *data)
{
	int				i;

	i = 0;
	while (tab[i])
		i++;
	if (i < 2)
		return (false);
	if (ft_is_str_n(tab[i - 2]) == false || ft_is_overflow(tab[i - 2]) == true)
		return (false);
	if (ft_is_str_n(tab[i - 1]) == false || ft_is_overflow(tab[i - 1]) == true)
		return (false);
	if (tab[0][0] == 'L' || tab[0][0] == '#')
		return (false);
	i -= 2;
	while (i > 0)
	{
		if (tab[i - 1] != NULL)
			data->next_name += (int)ft_strlen(tab[i - 1]) + 1;
		i--;
	}
	return (true);
}

static bool			check_double(char *id, t_data *data)
{
	t_room			*tmp;

	if (data->rooms == NULL)
		return (false);
	tmp = data->rooms;
	while (tmp->next)
	{
		if (ft_strequ(id, tmp->name))
			return (true);
		tmp = tmp->next;
	}
	if (ft_strequ(id, tmp->name))
		return (true);
	return (false);
}

bool				p_room(char *line, t_data *data)
{
	int				i;
	int				ret;
	char			**tab;

	i = 0;
	ret = 0;
	tab = ft_strsplit(line, ' ');
	while (tab[i])
		i++;
	if (i < 3)
	{
		(tab) ? ft_str_tabdel(&tab) : 0;
		return (false);
	}
	if (check_coord(tab, data) == false)
		ret = 1;
	if (ret != 1 && check_double(tab[0], data) == true)
		ret = 1;
	(tab) ? ft_str_tabdel(&tab) : 0;
	return ((ret == 0) ? true : false);
}

bool				m_room(char *line, t_data *data)
{
	t_room			*tmp;
	t_room			*node;

	if (!(node = (t_room *)malloc(sizeof(t_room) * 1)))
		return (false);
	node->name = ft_strnew((size_t)data->next_name + 1);
	ft_strncpy(node->name, line, (size_t)(data->next_name - 1));
	node->next = NULL;
	node->status = 0;
	node->type = data->next_type;
	data->next_type = NEUTRE;
	data->next_name = 0;
	if (data->rooms == NULL)
	{
		data->rooms = node;
		data->nb_room++;
		return (true);
	}
	tmp = data->rooms;
	while (tmp->next != NULL)
		tmp = tmp->next;
	tmp->next = node;
	data->nb_room++;
	return (true);
}
