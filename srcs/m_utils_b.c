/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_utils_b.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/20 17:09:52 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/22 16:27:15 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void			del_pipe(t_pipe *head)
{
	t_pipe			*tmp;

	tmp = head;
	while (tmp->next->next != NULL)
		tmp = tmp->next;
	if (tmp->next != NULL)
	{
		(tmp->next->in) ? ft_strdel(&tmp->next->in) : 0;
		(tmp->next->out) ? ft_strdel(&tmp->next->out) : 0;
		free(tmp->next);
		tmp->next = NULL;
	}
}

static void			del_room(t_room *head)
{
	t_room			*tmp;

	tmp = head;
	while (tmp->next->next != NULL)
		tmp = tmp->next;
	if (tmp->next != NULL)
	{
		(tmp->next->name) ? ft_strdel(&tmp->next->name) : 0;
		free(tmp->next);
		tmp->next = NULL;
	}
}

void				ft_kill_pipe(t_pipe *head)
{
	while (head->next != NULL)
		del_pipe(head);
	if (head->next != NULL)
	{
		(head->next->in) ? ft_strdel(&head->next->in) : 0;
		(head->next->out) ? ft_strdel(&head->next->out) : 0;
		free(head->next);
		head->next = NULL;
	}
	if (head != NULL)
	{
		(head->in) ? ft_strdel(&head->in) : 0;
		(head->out) ? ft_strdel(&head->out) : 0;
		free(head);
		head = NULL;
	}
}

void				ft_kill_room(t_room *head)
{
	while (head->next != NULL)
		del_room(head);
	if (head->next != NULL)
	{
		(head->next->name) ? ft_strdel(&head->next->name) : 0;
		free(head->next);
		head->next = NULL;
	}
	if (head != NULL)
	{
		(head->name) ? ft_strdel(&head->name) : 0;
		free(head);
		head = NULL;
	}
}

int					ft_seek_id(char *s, t_graph *graph)
{
	int				i;

	i = 0;
	while (graph->id[i])
	{
		if (ft_strequ(s, graph->id[i]))
			return (i);
		i++;
	}
	return (-1);
}
