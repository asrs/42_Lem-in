/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 18:39:34 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/22 16:30:34 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H

# include "libft.h"
# include <stdlib.h>
# include <unistd.h>
# include <stdbool.h>

# define END 1
# define START 0
# define NEUTRE 2

typedef struct				s_room
{
	char					*name;
	int						status;
	int						type;
	struct s_room			*next;
}							t_room;

typedef struct				s_pipe
{
	char					*in;
	char					*out;
	struct s_pipe			*next;
}							t_pipe;

typedef struct				s_data
{
	int						next_name;
	int						next_type;
	unsigned int			nb_room;
	unsigned int			nb_link;
	unsigned int			nb_ant;
	bool					get_ant;
	bool					get_start;
	bool					get_end;
	char					*readed;
	struct s_room			*rooms;
	struct s_pipe			*pipes;
}							t_data;

typedef struct				s_ant
{
	int						pos;
	char					*id;
}							t_ant;

typedef struct				s_graph
{
	int						**matr;
	char					**id;
	int						*path;
	int						pind;
	unsigned int			nb_ant;
	struct s_ant			*ants;
	size_t					len;
}							t_graph;

extern void					m_parse(t_data *data);
extern bool					m_room(char *line, t_data *data);
extern bool					m_pipe(char *line, t_data *data);
extern bool					p_room(char *line, t_data *data);
extern bool					p_pipe(char *line, t_data *data);
extern bool					m_valid(t_data *data);
extern void					m_process(t_data *data, t_graph *graph);
extern bool					m_algo(t_graph *graph);
extern void					m_print(t_graph *graph);

extern int					ft_seek_id(char *s, t_graph *graph);
extern void					ft_kill_room(t_room *head);
extern void					ft_kill_pipe(t_pipe *head);
extern bool					ft_is_str_n(char *line);
extern bool					ft_is_overflow(char *line);
extern void					ft_stock(char *line, t_data *data);
extern void					ft_message(int i, t_data *data, t_graph *graph);

#endif
